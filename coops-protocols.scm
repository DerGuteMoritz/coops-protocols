(module coops-protocols

((define-protocol make-protocol) protocol? extend-protocol
 protocol-extenders extend-class class-extends?
 satisfies? protocol-specs protocol-name)

(import chicken scheme)
(use coops srfi-1)

;; this is necessary due to a bug in coops, see ticket #478
(begin-for-syntax
 (import chicken)
 (use srfi-1))

(define-record protocol
  name specs (setter extenders))

(define-record-printer (protocol p out)
  (fprintf out "#<protocol ~A>" (protocol-name p)))

(define-syntax define-protocol
  (syntax-rules ()
    ((_ name
        (spec (this args ...))
        ...)
     (begin
       (define-generic (spec this))
       ...
       (define name
         (make-protocol
          'name
          '((spec this args ...)
            ...)
          '()))))))

(define-syntax extend-class
  (syntax-rules ()
    ((_ class
        (protocol
         (spec (this args ...)
               body ...)
         ...)
        ...)
     (begin
       (begin
         (set! (protocol-extenders protocol)
               (cons class (protocol-extenders protocol)))
         (define-method (spec (this class) args ...) body ...)
         ...)
       ...))))

(define-syntax extend-protocol
  (syntax-rules ()
    ((_ protocol
        (class
         (spec (this args ...)
               body ...)
         ...)
        ...)
     (begin
       (extend-class
        class
        (protocol
         (spec (this args ...)
               body ...)
         ...))
       ...))))

(define (class-extends? class protocol)
  (any (lambda (extender)
         (subclass? class extender))
       (protocol-extenders protocol)))

(define (satisfies? protocol object)
  (let ((class (class-of object)))
    (if (eq? #t class)
        (error 'satisfies? "not a coops instance" object)
        (class-extends? class protocol))))

)
