(use coops-protocols)
(use test)
(use coops coops-primitive-objects srfi-13 data-structures)

(define-protocol seq-access
  (head (this))
  (tail (this)))

(define-protocol seq-mod
  (prepend (this element)))

(extend-class
 <string>
 (seq-access
  (head
   (this)
   (string-ref this 0))
  (tail
   (this)
   (string-drop this 1)))
 (seq-mod
  (prepend
   (this el)
   (string-append el this))))

(extend-class
 <list>
 (seq-access
  (head
   (this)
   (car this))
  (tail
   (this)
   (cdr this)))
 (seq-mod
  (prepend
   (this el)
   (cons el this))))

(extend-protocol
 seq-access
 (<vector>
  (head (this)
        (vector-ref this 0))
  (tail (this)
        (vector-copy this 1)))
 (<queue>
  (head (this)
        (queue-first this))
  (tail (this)
        (cdr (queue->list this)))))


(test-group "satisfies?"
  (test-assert (satisfies? seq-access "foo"))
  (test-assert (satisfies? seq-mod "bar"))
  (test-assert (satisfies? seq-access '(1 2)))
  (test-assert (satisfies? seq-mod '()))
  (test-assert (not (satisfies? seq-mod display))))

(test-group "class-extends?"
  (test-assert (class-extends? <list> seq-access))
  (test-assert (class-extends? <pair> seq-access)) ; <pair> is a subclass of <list>
  (test-assert (not (class-extends? <char> seq-access)))
  (test-assert (class-extends? <string> seq-mod))
  (test-assert (class-extends? <queue> seq-access))
  (test-assert (not (class-extends? <queue> seq-mod))))

(test-group "protocol methods"
  (test #\f (head "foo"))
  (test 1 (head '(1 2 3)))
  (test '(1 2 3) (prepend '(2 3) 1))
  (test "abc" (prepend "bc" "a"))
  (test 1 (head (vector 1 2 3))))
